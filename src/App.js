import "./App.css";
import { useState, useEffect } from "react";
import {
  collection,
  query,
  orderBy,
  onSnapshot,
  addDoc,
  Timestamp,
  limit,
} from "firebase/firestore";
import { db } from "./firebase";

function App() {
  const [wishes, setWishes] = useState([]);
  const [disableWish, setDisableWish] = useState(false);
  const [message, setMessage] = useState("");
  const wishLocal = localStorage.getItem("mita wish");
  const date = new Date();
  const validBday = date.getDate() === 28 && date.getMonth() === 5;
  useEffect(() => {
    const q = query(
      collection(db, "wishes"),
      orderBy("created", "desc"),
      limit(10)
    );
    onSnapshot(q, (querySnapshot) => {
      setWishes(querySnapshot.docs.map((doc) => doc.data()));
    });
  }, []);

  useEffect(() => {
    if (wishLocal !== null) {
      setDisableWish(true);
    }
  }, [wishes, wishLocal]);
  const handleBdayWishes = async () => {
    try {
      await addDoc(collection(db, "wishes"), {
        message: message,
        created: Timestamp.now(),
      });
      localStorage.setItem("mita wish", true);
    } catch (err) {
      alert(err);
    }
  };
  return (
    <div className="app">
      {validBday ? (
        <div className="container">
          <div className="body">
            <div className="profile-pic-container">
              <img
                src="https://media-exp2.licdn.com/dms/image/C5603AQHK2_d-f1GdJg/profile-displayphoto-shrink_800_800/0/1632651439206?e=1661990400&v=beta&t=QqmsxWIQgXRXmFkoEu-RZLbs3B9_vDmaB8O7NtbsF9g"
                alt="pelaku"
                className="profile-pic"
              />
            </div>
            <div className="body-text">
              <h1>HI, GUYS!</h1>
              <h2>NAMA GUE RAHMITA ANDINY DAN GUE HARI INI ULANG TAHUN!</h2>
              <h3>UCAPIN GUE ULTAH DONG, GUYS! BIAR GUE GA SEDIH T_T</h3>
              {!disableWish ? (
                <div className="form-container">
                  <p>MESSAGE</p>
                  <textarea
                    onChange={(e) => setMessage(e.target.value)}
                    className="message-box"
                  />
                </div>
              ) : null}
              <div className="button-container">
                <button
                  className={`button-wish ${
                    disableWish || !message.length ? "button-wish-disabled" : ""
                  }`}
                  disabled={
                    wishLocal !== null || disableWish || !message.length
                  }
                  onClick={() => {
                    setDisableWish(true);
                    handleBdayWishes();
                  }}
                >
                  {disableWish
                    ? "LU UDAH NGUCAPIN ULTAH 😎!"
                    : "Selamat Ulang Tahun, Mita! 🎉"}
                </button>
              </div>
              <div>
                <h1>YANG UDAH UCAPIN GUA ULTAH UDAH ADA : {wishes.length}</h1>
              </div>
              {wishes.length ? (
                wishes.map((item, index) => {
                  return (
                    <div className="message-container" key={index}>
                      <h4>{item.message}</h4>
                    </div>
                  );
                })
              ) : (
                <div>Ga ada yang ngucapin huhu sedih 😭</div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div className="not-bday-container">
          <h1>Si Mita belum ultah guys!</h1>
        </div>
      )}
    </div>
  );
}

export default App;
